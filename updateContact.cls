List<Contact> conList = new List<Contact>();
conList.add(con);
SObjectAccessDecision contactSecurityDecisionUpdate = Security.stripInaccessible(AccessType.UPDATABLE, conList);
update contactSecurityDecisionUpdate.getRecords();   